## Getting started

```bash

# 1
git init ~

# 2
export GIT_DISCOVERY_ACROSS_FILESYSTEM=true

# 3
git remote add origin https://gitlab.com/school_bilibili/python/basictutorial011.git

# 4
git fetch

# 5
git branch --set-upstream-to=origin/main main

# 6
git stash push --quiet && git checkout main -f && git stash pop --quiet

# 7
git pull --rebase

```

